'use strict';

import { EventEmitter } from 'events';

let ipc:any = require('ipc');

export default class IPC extends EventEmitter {
    private _id:string;

    get id():string {
        return this._id;
    }

    constructor(id:string='ipc', retry:number=1500) {
        super();

        this._id = id;

        ipc.config.id = id;
        ipc.config.retry = retry;

        this.listen().start();
    }

    listen(event:string='linting.queue.updated'):IPC {
        ipc.serve(() => {
            ipc.server.on(event, (data:any, socket:any) => {
                this.emit('message', data);
            })
        });

        return this.start();
    }

    private start():IPC {
        ipc.server.start();

        return this;
    }
};