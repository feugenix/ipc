#!/bin/bash
set -e

BIN_DIR="$(npm bin)"

$BIN_DIR/typings install --ambient
$BIN_DIR/tsc