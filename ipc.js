'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var events_1 = require('events');
var ipc = require('ipc');
var IPC = (function (_super) {
    __extends(IPC, _super);
    function IPC(id, retry) {
        if (id === void 0) { id = 'ipc'; }
        if (retry === void 0) { retry = 1500; }
        _super.call(this);
        this._id = id;
        ipc.config.id = id;
        ipc.config.retry = retry;
        this.listen().start();
    }
    Object.defineProperty(IPC.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    IPC.prototype.listen = function (event) {
        var _this = this;
        if (event === void 0) { event = 'linting.queue.updated'; }
        ipc.serve(function () {
            ipc.server.on(event, function (data, socket) {
                _this.emit('message', data);
            });
        });
        return this.start();
    };
    IPC.prototype.start = function () {
        ipc.server.start();
        return this;
    };
    return IPC;
}(events_1.EventEmitter));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = IPC;
;
